﻿using System;

namespace tp2partie2Console
{
    public class Program
    {
        public static string recherche(int[] tableau, int nbrrecher)
        {
            string phrase1="sa place: valeur ", phrase2="ses places: valeurs",espace=" ", final="";
            int[] stc= new int[10];
            int compteur, cpt2=0;
            
            for (compteur=0;compteur<10;compteur++)
            {
                if (tableau[compteur]==nbrrecher)
                {
                    stc[cpt2]=compteur+1;
                    cpt2++;
                }
            }

            if (cpt2==0)
            {
                final="non présent";
            }

            else
            {
                if (cpt2==1)
                {
                    final=phrase1+stc[0];
                }

                if (cpt2>=2)
                {
                    final=phrase2;

                    for(compteur=0;compteur<cpt2;compteur++)
                    {
                        final+=espace+stc[compteur];
                    }

                }

            }

            return final;
            
        }

        static void Main(string[] args)
        {
            bool trye;
            string final="", val="";
            int[] stock=new int[10];
            int valcher, compteur;


            for(compteur=0;compteur<10;compteur++)
            {
                Console.WriteLine("Choisissez la valeur");
                val=Console.ReadLine();

                if(int.TryParse(val,out stock[compteur]))
                {
                    trye=true;
                }

                else
                {
                    trye=false;
                }

                while (trye==false)
                {
                    Console.WriteLine("erreur, rentrez à nouveau une valeur");
                    val=Console.ReadLine();

                    if(int.TryParse(val,out stock[compteur]))
                    {
                        trye=true;
                    }

                    else
                    {
                        trye=false;
                    }

                }

            }

            Console.WriteLine("Quelle est la valeur que vous recherchez ?");
            val=Console.ReadLine();

            if(int.TryParse(val,out valcher))
            {
                trye=true;
            }

            else
            {
                trye=false;
            }

            while (trye==false)
            {
                Console.WriteLine("erreur, rentrez à nouveau la valeur recherché");
                val=Console.ReadLine();

                if(int.TryParse(val,out valcher))
                {
                    trye=true;
                }

                else
                {
                    trye=false;
                }

            }



            final=recherche(stock, valcher);
            Console.WriteLine(final);
        }
    }
}
