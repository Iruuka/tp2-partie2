using System;
using Xunit;
using tp2partie2Console;
namespace tp2partie2Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal("sa place: valeur 3", Program.recherche(new int[]{3,5,9,4,6,1,3,7,8,0},9));
            Assert.Equal("non présent", Program.recherche(new int[]{3,0,6,8,9,7,5,1,2,0},4));
            Assert.Equal("ses places: valeur 5 8", Program.recherche(new int[] {5,7,9,4,1,3,6,1,2,0},1));
        }
    }
}
